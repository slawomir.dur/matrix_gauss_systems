#include "matrix.h"
using namespace std;

int main()
{
    const int number = 3;

    int errs = 0;

    GoldenPattern<double> gp = GoldenPatternFactory<double>::Create(3);

    std::shared_ptr<InfoDependencyTable> table { std::make_shared<InfoDependencyTable>() };

    auto elimin = gp.GetPattern().gauss_elimination(table);

    cout << *table;
    table->assemble();          

    
           

    return 0;
}
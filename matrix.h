#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <chrono>
#include <vector>
#include <random>
#include <map>
#include <algorithm>
#include <exception>
#include <memory>
#include <string>

using std::cout;
using std::endl;

class MatrixError : std::exception
{
    private:
        std::string msg;
    public:
        MatrixError(const std::string& msg) : msg(msg) {}
        virtual ~MatrixError() {}
        virtual const char* what() {return msg.c_str();}
        std::string getMessage() {return msg;}

};

class InfoDependencyTable;

template <typename T>
class Matrix;

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& mat);

template <typename T>
class Matrix
{
    private:
        T** data = nullptr;
        int rows;
        int cols;
        std::chrono::duration<int,std::ratio<1,1000000000>> gauss_time;
        bool debug { false };
    public:
        Matrix(int size, T initial_value = T());
        Matrix(int rows, int cols, T initial_value);
        Matrix(const std::vector<std::vector<T>>& matrix_data);
        Matrix(const Matrix& other);
        Matrix(Matrix&& other) noexcept;
        ~Matrix();
        Matrix& operator=(const Matrix& other);
        Matrix& operator=(Matrix&& other) noexcept;
        Matrix operator+(const Matrix& other);
        Matrix operator-(const Matrix& other);
        Matrix operator*(const Matrix& other);
        bool operator==(const Matrix& other);
        bool operator!=(const Matrix& other) { return !(*this == other); }
        void reset(int size, T value = T());
        void reset(int rows, int cols, T value = T());
        T get(int row, int col) const { return data[row][col]; }
        void set(int row, int col, T value) { data[row][col] = value; }
        int get_rows() const { return rows; }
        int get_cols() const { return cols; }
        Matrix transpose() const;
        Matrix gauss_elimination(std::shared_ptr<InfoDependencyTable> table = std::shared_ptr<InfoDependencyTable>());
        template <typename TIME_CAST>
        long long last_gauss_time_to() { return std::chrono::duration_cast<TIME_CAST>(gauss_time).count(); }
        void setDebug(bool debug);
        friend std::ostream& operator<<<T>(std::ostream& os, const Matrix<T>& mat);
    private:
        void allocate();
        void deallocate();
        void init_with_value(T value);
};


typedef std::pair<int, int> Conn;

class Vertex;

class VertexConnections
{
private:
    Conn kDirectedConnection;
    Conn jDirectedConnection;
    Conn iDirectedConnection;

    std::vector<const Vertex*> connected;

public:
    VertexConnections() = default;
    void setKDirectedConnection(int j, int i) { kDirectedConnection = std::make_pair(j, i); }
    void setJDirectedConnection(int i, int k) { jDirectedConnection = std::make_pair(i, k); }
    void setIDirectedConnection(int j, int k) { iDirectedConnection = std::make_pair(j, k); }

    void addConnection(const Vertex* vertex) { connected.push_back(vertex); }
    std::vector<const Vertex*> getConnections() const { return connected; }
    

    Conn getKDirectedConnection() const { return kDirectedConnection; }
    Conn getJDirectedConnection() const { return jDirectedConnection; }
    Conn getIDirectedConnection() const { return iDirectedConnection; }

    std::tuple<Conn, Conn, Conn> GetConnections() const { return std::make_tuple(kDirectedConnection, jDirectedConnection, iDirectedConnection); }
};

class Vertex
{
private:
    std::tuple<int, int, int> position;
    VertexConnections connections;
public:
    void setPosition(int i, int j, int k) { position = std::make_tuple(i, j, k); }
    void setKDirectedConnection(int j, int i) { connections.setKDirectedConnection(j, i); }
    void setJDirectedConnection(int i, int k) { connections.setJDirectedConnection(i, k); }
    void setIDirectedConnection(int j, int k) { connections.setIDirectedConnection(j, k); }

    void addConnection(const Vertex* vertex) { connections.addConnection(vertex); }
    std::vector<const Vertex*> getConnectedVertices() const { return connections.getConnections(); }

    std::tuple<int, int, int> getPosition() const { return position; }
    Conn getKDirectedConnection() const { return connections.getKDirectedConnection(); }
    Conn getJDirectedConnection() const { return connections.getJDirectedConnection(); }
    Conn getIDirectedConnection() const { return connections.getIDirectedConnection(); }

    std::tuple<Conn, Conn, Conn> getConnections() const { return connections.GetConnections(); }

    friend std::ostream& operator<<(std::ostream& os, const Vertex& vertex);
};

std::ostream& operator<<(std::ostream& os, const Vertex& vertex) 
{
    auto connToString = [](const Conn& conn) -> std::string {
        return '(' + std::to_string(conn.first) + ',' + std::to_string(conn.second) + ')';
    };
    auto [i, j, k] = vertex.position;
    auto [kConn, jConn, iConn] = vertex.getConnections();
    return os << "Position(i: " << i << ", j: " << j << ", k: " << k << "), " << connToString(kConn) << ", " << connToString(jConn) << ", " << connToString(iConn);
}

class InfoDependencyTable
{
private:
    std::vector<Vertex> vertices;
public:
    void pushVertex(const Vertex& vertex) { vertices.push_back(vertex); }
    size_t size() { return vertices.size(); }
    Vertex getVertex(int index) { return vertices.at(index); }
    void sort() {
        std::sort(vertices.begin(), vertices.end(), [](const Vertex& v1, const Vertex& v2) {
            auto [v1i, v1j, v1k] = v1.getPosition();
            auto [v2i, v2j, v2k] = v2.getPosition();
            if (v1i != v2i)
            {
                return v1i < v2i;
            }
            else if (v1j != v2j)
            {
                return v1j < v2j;
            }
            else
            {
                return v1k < v2k;
            }
            });
    }

    void assemble();

    friend std::ostream& operator<<(std::ostream& os, const InfoDependencyTable& table);
};

std::ostream& operator<<(std::ostream& os, const InfoDependencyTable& table)
{
    for (auto&& vert : table.vertices)
    {
        os << vert << std::endl;
    }
    return os;
}

void InfoDependencyTable::assemble()
{
    sort();
    
    // first i k connections, then j i, then j k, then cross jk to ik
    Conn connection;
    for (int i = 0; i < 4; ++i)
    {
        for (auto primIt = vertices.begin(); primIt != vertices.end(); ++primIt)
        {
            auto idxToConn = [&](const Vertex& vertex, int idx, bool isCrossed = false) -> Conn {
                switch (i) {
                    case 0: return vertex.getJDirectedConnection();
                    case 1: return vertex.getKDirectedConnection();
                    case 2: return vertex.getIDirectedConnection();
                    case 3: return ((isCrossed) ? vertex.getJDirectedConnection() : vertex.getIDirectedConnection());
                    default: throw std::runtime_error("");
                }
            };

            connection = idxToConn(*primIt, i);

            auto found = std::find_if(primIt + 1, vertices.end(), [&](const Vertex& vertex) {
                auto [v1, v2] = idxToConn(vertex, i, true);
                auto [pv1, pv2] = connection;
                return (v1 == pv1) && (v2 == pv2);
                });
            if (found != vertices.end())
            {
                primIt->addConnection(&(*found));
            }
        }
    }
}

template <typename T>
void Matrix<T>::allocate()
{
    this->data = new T*[this->rows];
    for (int i = 0; i < this->rows; ++i)
    {
        this->data[i] = new T[this->cols];
    }
}

template <typename T>
void Matrix<T>::deallocate()
{
    if (this->data == nullptr)
        return;
    for (int i = 0; i < this->rows; ++i)
    {
        delete[] this->data[i];
    }
    delete[] this->data;
}

template <typename T>
void Matrix<T>::init_with_value(T value)
{
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            this->data[i][j] = value;
        }
    }
}

template <typename T>
Matrix<T>::Matrix(int size, T initial_val) : rows(size), cols(size)
{
    allocate();
    init_with_value(initial_val);
}

template <typename T>
Matrix<T>::Matrix(int rows, int cols, T initial_val) : rows(rows), cols(cols)
{
    allocate();
    init_with_value(initial_val);
}

template <typename T>
Matrix<T>::Matrix(const std::vector<std::vector<T>>& matrix_data)
{
    const int rows = matrix_data.size();
    const int cols = matrix_data.begin()->size();

    this->rows = rows;
    this->cols = cols;

    allocate();

    int row {};
    for (typename std::vector<std::vector<T>>::const_iterator row_it = matrix_data.cbegin(); row_it != matrix_data.cend(); ++row_it)
    {
        typename std::vector<T>::const_iterator col_it = row_it->cbegin();
        if (row_it->size() != cols)
            throw std::runtime_error("Matrix size is not stable");
        int col {};
        for (; col_it != row_it->cend(); ++col_it)
        {
            this->data[row][col] = *col_it;
            ++col;
        }
        ++row;
    }
}

template <typename T>
Matrix<T>::Matrix(const Matrix& other) : rows(other.rows), cols(other.cols)
{
    allocate();
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            this->data[i][j]=other.data[i][j];
        }
    }
}

template <typename T>
Matrix<T>::Matrix(Matrix&& other) noexcept : rows(other.rows), cols(other.cols)
{
    this->data = other.data;
    other.data = nullptr;
}

template <typename T>
Matrix<T>::~Matrix()
{
    deallocate();
}

template <typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix& other)
{
    if (this == &other)
        return *this;
    
    deallocate();

    this->rows = other.rows;
    this->cols = other.cols;

    allocate();
    
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            this->data[i][j]=other.data[i][j];
        }
    }

    return *this;
}

template <typename T>
Matrix<T>& Matrix<T>::operator=(Matrix&& other) noexcept
{
    if (this == &other)
        return *this;
    this->data = other.data;
    other.data = nullptr;
}

template <typename T>
Matrix<T> Matrix<T>::operator+(const Matrix& other)
{
    Matrix<T> result(this->rows, this->cols, T());
    for (int row = 0; row < result.rows; ++row)
    {
        for (int col = 0; col < result.cols; ++col)
        {
            result.data[row][col] = this->data[row][col] + other.data[row][col];
        }
    }
    return result;  
}

template <typename T>
Matrix<T> Matrix<T>::operator-(const Matrix& mat)
{
    Matrix<T> result(this->rows, this->cols, T());

    for (int row = 0; row < result.rows; ++row)
    {
        for (int col = 0; col < result.cols; ++col)
        {
            result.data[row][col] = this->data[row][col] - mat.data[row][col];
        }
    }

    return result;
}

template <typename T>
Matrix<T> Matrix<T>::operator*(const Matrix& mat)
{
    Matrix<T> result(this->rows, this->cols, T());

    for (int row = 0; row < this->rows; ++row)
    {
        for (int col = 0; col < this->cols; ++col)
        {
            for(int k = 0; k < this->rows; ++k)
                result.data[row][col] += this->data[row][k] * mat.data[k][col];
        }
    }
    return result;
}

template <typename T>
bool Matrix<T>::operator==(const Matrix<T>& other)
{
    if (this->rows != other.rows || this->cols != other.cols)
        return false;
    for (int i = 0; i < this->rows; ++i)
    {
        for (int j = 0; j < this->cols; ++j)
        {
            if (this->data[i][j] != other.data[i][j])
                return false;
        }
    }
    return true;
}

template <typename T>
void Matrix<T>::reset(int size, T value)
{
    deallocate();
    
    this->rows = size;
    this->cols = size;
    
    allocate();
    init_with_value(value);
}

template <typename T>
void Matrix<T>::reset(int rows, int cols, T value)
{
    deallocate();

    this->rows = rows;
    this->cols = cols;

    allocate();
    init_with_value(value);
}

template <typename T>
Matrix<T> Matrix<T>::transpose() const
{
    Matrix result {this->cols, this->rows, T()};
    for (int row = 0; row < this->rows; ++row)
    {
        for (int col = 0; col < this->cols; ++col)
        {
            result.set(col, row, this->data[row][col]);
        }
    }
    return result;
}

template <typename T>
Matrix<T> Matrix<T>::gauss_elimination(std::shared_ptr<InfoDependencyTable> table)
{
    if (this->rows+1 != this->cols)
        throw std::domain_error("Not a proper matrix");
    const int N = this->rows;
    
    Matrix<T> outArr(*this);
    Matrix<T> m(N,N+1,T());

    auto start = std::chrono::high_resolution_clock::now();

    try
    {
        cout << " nr | w1  | w2  | w3  | Im  | Ia2 | Ia1 \n";
        cout << "    |  i  |  j  |  k  | j i | i k | j k \n\n";
        int counter{};
        int counter2{};
        for (int i = 0; i < N-1; ++i)
        {
            for (int j = i + 1; j < N; ++j)
            {
                if (this->data[i][i] != 0)
                {
                    m.data[j][i] = -outArr.data[j][i] / outArr.data[i][i];
                    if (table.get() != nullptr)
                    {
                        Vertex vertex{};
                        vertex.setPosition(i, j, i);
                        vertex.setKDirectedConnection(j, i);
                        vertex.setJDirectedConnection(i, i);
                        vertex.setIDirectedConnection(j, i);
                        table->pushVertex(vertex);
                    }
                    cout << "  " << counter2 << " |  " << i << "  |  " << j << "  |  " << i << "  | " << j << " " << i << " | " << i << " " << i << " | " << j << " " << i << endl;
                    ++counter2;
                }
                else
                {
                    m.data[j][i] = 0;
                }
            }
            for (int j = i+1; j < N; ++j)
            {
                for (int k = i+1; k <= N; ++k)
                {
                    outArr.data[j][k-1] += m.data[j][i] * outArr.data[i][k-1];
                    if (table.get() != nullptr)
                    {
                        Vertex vertex{};
                        vertex.setPosition(i, j, k);
                        vertex.setKDirectedConnection(j, i);
                        vertex.setJDirectedConnection(i, k);
                        vertex.setIDirectedConnection(j, k);
                        table->pushVertex(vertex);
                    }
                    cout << "  " << counter << " |  " << i << "  |  " << j << "  |  " << k << "  | " << j << " " << i << " | " << i << " " << k << " | " << j << " " << k << endl;
                    ++counter;
                }
            }
        }

    }
    catch (...)
    {
        throw MatrixError("Internal error during gauss elimination");
    }

    auto stop = std::chrono::high_resolution_clock::now();
    auto time = stop - start;
    this->gauss_time = time;

    return outArr;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& mat)
{
    for(int i = 0; i < mat.get_rows(); ++i){
        for (int j = 0; j < mat.get_cols(); ++j){
            os << mat.get(i,j) << " ";
        }
        os << std::endl;
    }
    return os;
}

template <typename T>
class GoldenPatternFactory;

template <typename T>
class GoldenPattern
{
    friend GoldenPatternFactory<T>;
    private:
        Matrix<T> pattern;
        Matrix<T> solution;
    public:
        Matrix<T> GetPattern() {return pattern;}
        Matrix<T> GetSolution() {return solution;}
    private:
        GoldenPattern(const Matrix<T>& pattern, const Matrix<T>& solution) : pattern(pattern), solution(solution) {}
};


template <typename T>
class GoldenPatternFactory
{
    public:
        static GoldenPattern<T> Create(int size);
};

template <typename T>
GoldenPattern<T> GoldenPatternFactory<T>::Create(int size)
{
    Matrix<T> goldenMatrix {size, size+1, T()};

    unsigned seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::default_random_engine engine(seed);
    std::uniform_int_distribution<int> distribution(1,5);

    typename std::map<int, T> answers; 

    // make diagonal matrix and generate random answers
    for (int i = 0; i < goldenMatrix.get_rows(); ++i)
    {
        // make diagonal with 1.0
        goldenMatrix.set(i,i,static_cast<T>(1.0));
        // generate values in the answers vector
        T answer = static_cast<T>(distribution(engine));
        answers.insert(std::make_pair(i,answer));
        goldenMatrix.set(i,goldenMatrix.get_cols()-1,answer);
    }

   Matrix<T> solution = goldenMatrix;

    for (int i = 0; i < goldenMatrix.get_rows(); ++i)
    {
        for (int j = 0; j < goldenMatrix.get_cols()-1; ++j)
        {
            // randomize values in matrix
            goldenMatrix.set(i,j,static_cast<T>(distribution(engine)));
            // calculate answers for each row
            T answerForRow = T();
            for (int k = 0; k < goldenMatrix.get_cols()-1; ++k)
            {
                try
                {
                    answerForRow += goldenMatrix.get(i,k) * answers.at(k);
                }
                catch(const std::out_of_range& e)
                {
                    std::cerr << "Internal error of Matrix";
                    throw;
                }            
            }
            goldenMatrix.set(i, goldenMatrix.get_cols()-1, answerForRow);
        }
    }
    return GoldenPattern<T>(goldenMatrix, solution);
}



#endif
